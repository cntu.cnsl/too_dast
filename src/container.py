# Support Optional
from __future__ import annotations

from typing import Optional

from src.configuration import Configuration
from src.scan_script_wrapper import ScanScriptWrapper


class Container:
    # Either cache is Container or None
    CACHE: Optional[Container] = None

    scan: ScanScriptWrapper
    config: Configuration
