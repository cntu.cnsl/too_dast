# too_dast Simple DAST tools (CodeReports Project - 1)

This is my project for training professional coding.
The main purpose is to help entry-level developers to improve the
coding skills by following the leading edge project.

# Target Project
It is not a forked version but a copycat (with custom) of dast
tool from gitlab
[gitlab DAST](https://gitlab.com/gitlab-org/security-products/dast/)

# Report For Training
This one is a report for the project.

The report provides notes, tips \& tricks during the Python project development

## Preparation
* After gitlab project is cloned, the README.md files should be filled with some information
 related to the project
* Create virtualenv with: python3 -m venv .
* Add .gitignore to ignore all files created by virtualenv
* Create [.editorconfig](https://editorconfig.org/) file for coding style (Copycat from gitlab dast).
     Since I used neovim for coding, I need to add the vim-plug config into my configuration
* Create mypy.ini file to support static type checker

## Development
* The main.py will import the functions from src folder and others
* The dast.py will take any Callable() object that return Container object

### Error Handler System
The error handler class should be consider to catch and handle specific errors in an appropriate manner.
* Make an Error Handle system that can catch whatever error cases that we can thing of.
* Each module will contain their own error handle system. e.g. Config module contains Invalid configuration erorr handle.

### Logging Handler System
The logging system should divided into at least 2 types: DEBUG and INFO. The type can be modified by config.

The following components should contain logging system:
* Configuration

## CI/CD
Continuous Integration / Continuous Development should be prepared (.gitlab-ci.yaml)
There are many possible states such as Build, Run, Test. One can add as much as possible. In this repository, there are additional stages:
* Notify: Send a commit failed to slack :)
* Clean: Clean the registry

For the earliest version of simple DAST, only build, run, test are consider. (Will be extended later)

## Conclusion

| Item         | Pros                                                                                                                                                                        | Cons                                                                              |
|--------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------|
| Design       | Worked as Compose Pattern where the container object share the same action on DAST (We can add different type of container objects without affecting the DAST architecture) | No packaging tools such as setup. Only environment preparation (requirements.txt) |
| CI/CD        | CI/CD Stages are well-organized                                                                                                                                             | CI/CD is complicated with shell scripts and Dockers                               |
| Code Check   | Include a lot of code quality and build checks: pylint, pytest, shellcheck                                                                                                  |                                                                                   |
| Coding Style | Follows the type hints with some usage on __future__, Optional, and more                                                                                                    |                                                                                   |
