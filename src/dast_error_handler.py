import logging

from src.config import InvalidConfigurationError
#from src.models.error import InvalidAPISpecificationError, InvalidTargetError, NoUrlsError, TargetNotAccessibleError

#
# Project needs to handle some error nicely without STACKSTRACE
#
class DASTErrorHandler:

    ERRORS_TO_PRINT_WITHOUT_STACKSTRACE = [
            # InvalidAPISpecificationError,
            InvalidConfigurationError,
            # InvalidTargetError,
            # NoUrlsError,
            # TargetNotAccessibleError,
    ]

    def handle(self, error: Exception) -> None:

        if [class_name for class_name in self.ERRORS_TO_PRINT_WITHOUT_STACKSTRACE if isinstance(error, class_name)]:
            logging.error(f'{type(error).__name__}: {error}')
            return

        logging.error('Unhandled exception has been thrown, aborting.', exc_info=error)


