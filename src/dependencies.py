import os
import sys
# import uuid

from src.system import System
# from src.config import ConfigurationParser
from src.container import Container
# from src.custom_hooks import CustomHooks
from src.scan_script_wrapper import ScanScriptWrapper


def initialize_container() -> Container:
    # Return a cached
    if Container.CACHE:
        return Container.CACHE

    c = Container()
    c.system = System()
#    c.start_date_time = c.system.current_date_time()
    c.config = None
#     c.config = ConfigurationParser().parse(sys.argv[1:], os.environ)
    c.zap_debug = None

    c.scan = ScanScriptWrapper()
    Container.CACHE = c
    return Container.CACHE
