from __future__ import annotations

import pprint
# import socket
import sys
# from datetime import datetime
# from datetime import timezone
# from subprocess import PIPE, Popen
# from typing import List, Optional


class System:
    # DAST_VERSION = Optional[str] = None
    DAST_VERSION = "1"

    def __init__(self):
        self.pretty_print = pprint.PrettyPrinter(indent=4)

    def pretty_notify(self, message):
        print(message)

    def dast_version(self) -> str:
        return System.DAST_VERSION

    def python_version(self) -> str:
        return ' '.join([x.strip() for x in sys.version.splitlines()])

