from typing import Any, Dict, List, Optional

# @TBA We will update the model later

# from .models import Limits


class Configuration:

    def __init__(self, settings: Any):
        self.config1: bool = settings.config1
        self.zap_debug: Optional[bool] = settings.zap_debug
